locals {
  az_region = "westeurope"
  tenant_id = "12345678"

  tags = {
    CreatedBy    = "Terraform"
    CustomerName = "Ascode"
    Environment  = var.environment
    Project      = "Azure Virtual Desktops"
  }

  keyvault_admins = {
    erikchristiaans = data.azuread_user.erik_christiaans.id
  }

  pooled_desktop_users = {
    erikchristiaans = data.azuread_user.erik_christiaans.id
  }
  demo_desktop_users = {
    erikchristiaans = data.azuread_user.erik_christiaans.id
  }

  pooled_desktop_admins = {
    erikchristiaans = data.azuread_user.erik_christiaans.id
  }

  demo_desktop_admins = {
    erikchristiaans = data.azuread_user.erik_christiaans.id
  }

}

data "azuread_user" "erik_christiaans" {
  user_principal_name = "erik@ascode.nl"
}
