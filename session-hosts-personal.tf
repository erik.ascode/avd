# To get a list of Desktop VM Images, 'az vm image list -p "MicrosoftWindowsDesktop" --all -otable'
locals {
  w10_personal = {
    name           = "wxpersonal"
    computer_name  = "wxpersonal"
    subnet_id      = var.subnet_id
    disk_size_gb   = 100
    instance_size  = "Standard_D2s_v4"
    admin_username = random_string.admin_user["w10_personal"].result
    admin_password = random_password.admin_password["w10_personal"].result
    source_image_reference = {
      publisher = "MicrosoftWindowsDesktop"
      offer     = "office-365"
      sku       = "21h1-evd-o365pp"
      version   = "latest"
    }
  }

  personal_count = 0

}

# Virtual Machines
# Personal Session Host(s)
resource "azurerm_network_interface" "nic_w10_personal" {
  count               = local.personal_count
  name                = "${var.environment}-${local.w10_personal.name}-${count.index + 1}-nic"
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = local.w10_personal.subnet_id
    private_ip_address_allocation = "Dynamic"
  }

  tags = local.tags
}

resource "azurerm_windows_virtual_machine" "w10_personal" {
  count               = local.personal_count
  depends_on          = [azurerm_key_vault_access_policy.disk_access, azurerm_disk_encryption_set.desktops_ka, null_resource.encryption_at_host]
  name                = "${var.environment}-${local.w10_personal.name}-${count.index + 1}"
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name

  size           = local.w10_personal.instance_size
  admin_username = local.w10_personal.admin_username
  admin_password = local.w10_personal.admin_password

  network_interface_ids = [
    azurerm_network_interface.nic_w10_personal[count.index].id,
  ]

  os_disk {
    name                   = "${var.environment}-${local.w10_personal.name}-${count.index + 1}-OSDisk"
    caching                = "ReadWrite"
    storage_account_type   = "Premium_LRS"
    disk_encryption_set_id = azurerm_disk_encryption_set.desktops_ka.id
  }

  source_image_reference {
    publisher = local.w10_personal.source_image_reference.publisher
    offer     = local.w10_personal.source_image_reference.offer
    sku       = local.w10_personal.source_image_reference.sku
    version   = local.w10_personal.source_image_reference.version
  }

  boot_diagnostics {
    storage_account_uri = trimsuffix(tostring(azurerm_storage_account.desktops_ka.primary_blob_endpoint), "/${var.environment}-${local.w10_personal.name}-${count.index + 1}")
  }

  identity {
    type = "SystemAssigned"
  }

  timezone = "W. Europe Standard Time"

  tags                       = local.tags
  encryption_at_host_enabled = true
}

resource "azurerm_monitor_diagnostic_setting" "w10_personal" {
  count                      = local.personal_count
  name                       = "${azurerm_windows_virtual_machine.w10_personal[count.index].name}-diag"
  target_resource_id         = azurerm_windows_virtual_machine.w10_personal[count.index].id
  storage_account_id         = azurerm_storage_account.ka_analytics.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.ka_analytics.id

  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = true
      days    = var.environment != "prd" ? 30 : 365
    }
  }
}

resource "azurerm_virtual_machine_extension" "w10_personal_session_host_log_analytics" {
  count                = local.personal_count
  depends_on           = [azurerm_virtual_machine_extension.join_domain_w10_personal]
  name                 = "${local.w10_personal.name}-${count.index + 1}-log-analytics"
  virtual_machine_id   = azurerm_windows_virtual_machine.w10_personal[count.index].id
  publisher            = "Microsoft.EnterpriseCloud.Monitoring"
  type                 = "MicrosoftMonitoringAgent"
  type_handler_version = "1.0"

  settings = <<SETTINGS
      {
          "workspaceId": "${azurerm_log_analytics_workspace.ka_analytics.workspace_id}"
      }
  SETTINGS

  protected_settings = <<PROTECTED_SETTINGS
      {
          "workspaceKey": "${azurerm_log_analytics_workspace.ka_analytics.primary_shared_key}"
      }
  PROTECTED_SETTINGS
}

resource "azurerm_virtual_machine_extension" "w10_personal_session_host_anti_malware" {
  count                = local.personal_count
  depends_on           = [azurerm_virtual_machine_extension.join_domain_w10_personal]
  name                 = "${local.w10_personal.name}-${count.index + 1}-anti-malware"
  virtual_machine_id   = azurerm_windows_virtual_machine.w10_personal[count.index].id
  publisher            = "Microsoft.Azure.Security"
  type                 = "IaaSAntimalware"
  type_handler_version = "1.3"

  settings = <<SETTINGS
      {
          "AntimalwareEnabled": "true",
          "Exclusions": {
            "Extensions": ".log;.ldf",
            "Paths": "%windir%\\SoftwareDistribution\\Datastore\\Edb*.jrs;%windir%\\SoftwareDistribution\\Datastore\\Edb.chk;%windir%\\SoftwareDistribution\\Datastore\\Tmp.edb;%windir%\\Security\\Database\\*.edb;%windir%\\Security\\Database\\*.sdb;%windir%\\Security\\Database\\*.log;%windir%\\Security\\Database\\*.chk;%windir%\\Security\\Database\\*.jrs;%windir%\\Security\\Database\\*.xml;%windir%\\Security\\Database\\*.csv;%windir%\\Security\\Database\\*.cmtx;%allusersprofile%\\NTUser.pol;%SystemRoot%\\System32\\GroupPolicy\\Machine\\Registry.pol;%SystemRoot%\\System32\\GroupPolicy\\Machine\\registry.tmp;%SystemRoot%\\System32\\GroupPolicy\\User\\registry.pol;%SystemRoot%\\System32\\GroupPolicy\\User\\registry.tmp;userprofile%\\NTUser.dat*",
            "Processes": "mssence.svc"
          },
          "RealtimeProtectionEnabled": "true",
          "ScheduledScanSettings": {
            "isEnabled": "true",
            "scanType": "Quick",
            "day": "7",
            "time": "120"
          }
      }
  SETTINGS
}

resource "azurerm_virtual_machine_extension" "windows10_config_demo" {
  count                = local.personal_count
  depends_on           = [azurerm_virtual_machine_extension.join_domain_w10_personal]
  name                 = "${local.w10_personal.name}-${count.index + 1}-config"
  virtual_machine_id   = azurerm_windows_virtual_machine.w10_personal[count.index].id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.10"

  settings = <<SETTINGS
  { 
      "fileUris": [
          "${azurerm_storage_blob.windows10_config_demo[0].url}"
      ],
      "commandToExecute": "powershell.exe -ExecutionPolicy Unrestricted -file windows10ConfigDemo.ps1"
  }
  SETTINGS

  protected_settings = <<PROTECTEDSETTINGS
  {
      "storageAccountName": "${azurerm_storage_account.desktops_ka.name}",
      "storageAccountKey": "${azurerm_storage_account.desktops_ka.primary_access_key}"
  }
  PROTECTEDSETTINGS

  tags = local.tags
}
