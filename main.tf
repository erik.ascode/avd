terraform {
  backend "gcs" {
    bucket = "tf-statefile-backend"
    prefix = "asc/infra/avd"
  }
}
