resource "azurerm_key_vault" "desktops_ka" {
  name                        = "${var.customer_tla}${var.environment}dtka"
  location                    = azurerm_resource_group.desktops_ka.location
  resource_group_name         = azurerm_resource_group.desktops_ka.name
  tenant_id                   = local.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = true
  enabled_for_disk_encryption = true
  enabled_for_deployment      = true

  sku_name = "standard"

  tags = local.tags
}

# Key Vault Policies for Admin Users
resource "azurerm_key_vault_access_policy" "admin" {
  for_each = local.keyvault_admins

  key_vault_id = azurerm_key_vault.desktops_ka.id
  tenant_id    = local.tenant_id
  object_id    = each.value

  key_permissions = [
    "backup", "create", "decrypt", "delete", "encrypt", "get", "import", "list", "purge", "recover", "restore", "sign", "unwrapKey", "update", "verify", "wrapKey"
  ]

  certificate_permissions = ["backup", "create", "delete", "deleteissuers", "get", "getissuers", "import", "list", "listissuers", "managecontacts", "manageissuers", "purge", "recover", "restore", "setissuers", "update"]

  secret_permissions = [
    "backup", "delete", "get", "list", "purge", "recover", "restore", "set"
  ]
}

resource "azurerm_key_vault_key" "disk_encryption_key" {
  name         = "disk-encryption-key"
  key_vault_id = azurerm_key_vault.desktops_ka.id
  key_type     = "RSA"
  key_size     = "4096"

  depends_on = [
    azurerm_key_vault_access_policy.admin
  ]

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
  tags = local.tags
}

resource "azurerm_disk_encryption_set" "desktops_ka" {
  name                = "disk-encryption-set-wvd"
  resource_group_name = azurerm_resource_group.desktops_ka.name
  location            = azurerm_resource_group.desktops_ka.location
  key_vault_key_id    = azurerm_key_vault_key.disk_encryption_key.id

  identity {
    type = "SystemAssigned"
  }
}

resource "azurerm_role_assignment" "disk_encryption_set_reader" {
  scope                = azurerm_key_vault.desktops_ka.id
  role_definition_name = "Reader"
  principal_id         = azurerm_disk_encryption_set.desktops_ka.identity.0.principal_id
}

resource "azurerm_key_vault_access_policy" "disk_access" {
  key_vault_id = azurerm_key_vault.desktops_ka.id

  tenant_id = azurerm_disk_encryption_set.desktops_ka.identity.0.tenant_id
  object_id = azurerm_disk_encryption_set.desktops_ka.identity.0.principal_id

  key_permissions = [
    "get",
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}
