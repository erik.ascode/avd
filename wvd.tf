resource "azurerm_resource_group" "desktops_ka" {
  name     = "${var.environment}-desktops-ka"
  location = local.az_region

  tags = local.tags
}

# Provider registration is automatically done by Terraform
# resource azurerm_resource_provider_registration desktops {
#   name = "Microsoft.DesktopVirtualization"
# }

resource "azurerm_virtual_desktop_host_pool" "dt_pooled" {
  count               = local.pooled_count > 0 ? 1 : 0
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name

  name                     = "${var.environment}-desktops-ka-pooled-host-pool"
  friendly_name            = "${var.environment}-desktops-ka-pooled"
  validate_environment     = true # must be set to true for Azure AD joined VMs
  description              = "Proof of Concept: A pooled host pool - ${var.environment}-desktops-ka"
  type                     = "Pooled"
  maximum_sessions_allowed = 20
  load_balancer_type       = "BreadthFirst"
  custom_rdp_properties    = "use multimon:i:0;targetisaadjoined:i:1;"

  registration_info {
    expiration_date = "2022-03-28T22:59:00Z"
  }

  tags = local.tags
}

resource "azurerm_virtual_desktop_host_pool" "dt_personal" {
  count               = local.personal_count > 0 ? 1 : 0
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name

  name                             = "${var.environment}-desktops-ka-personal-host-pool"
  friendly_name                    = "${var.environment}-desktops-ka-personal"
  validate_environment             = true # must be set to true for Azure AD joined VMs
  description                      = "Proof of Concept: A personal host pool - ${var.environment}-desktops-ka"
  type                             = "Personal"
  load_balancer_type               = "Persistent"
  personal_desktop_assignment_type = "Automatic"
  preferred_app_group_type         = "Desktop"
  custom_rdp_properties            = "use multimon:i:0;targetisaadjoined:i:1;"

  registration_info {
    expiration_date = "2022-03-28T22:59:00Z"
  }

  tags = local.tags
}

resource "azurerm_virtual_desktop_host_pool" "dt_demo" {
  count               = local.demo_count > 0 ? 1 : 0
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name

  name                     = "${var.environment}-desktops-ka-demo-host-pool"
  friendly_name            = "${var.environment}-desktops-ka-demo"
  validate_environment     = true # must be set to true for Azure AD joined VMs
  description              = "Proof of Concept: A demo host pool - ${var.environment}-desktops-ka"
  type                     = "Pooled"
  maximum_sessions_allowed = 20
  load_balancer_type       = "BreadthFirst"
  custom_rdp_properties    = "use multimon:i:0;targetisaadjoined:i:1;"

  registration_info {
    expiration_date = "2022-03-28T22:59:00Z"
  }

  tags = local.tags
}

resource "azurerm_virtual_desktop_application_group" "desktop_pooled" {
  count                        = local.pooled_count > 0 ? 1 : 0
  name                         = "desktop-pooled"
  location                     = azurerm_resource_group.desktops_ka.location
  resource_group_name          = azurerm_resource_group.desktops_ka.name
  type                         = "Desktop"
  host_pool_id                 = azurerm_virtual_desktop_host_pool.dt_pooled.0.id
  default_desktop_display_name = "Ascode Virtual Desktop"
  friendly_name                = "Shared Desktops"
}

resource "azurerm_virtual_desktop_application_group" "desktop_personal" {
  count               = local.personal_count > 0 ? 1 : 0
  name                = "desktop-personal"
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name
  type                = "Desktop"
  host_pool_id        = azurerm_virtual_desktop_host_pool.dt_personal[0].id
  friendly_name       = "Personal Desktops"
}

resource "azurerm_virtual_desktop_application_group" "desktop_demo" {
  count                        = local.demo_count > 0 ? 1 : 0
  name                         = "desktop-demo"
  location                     = azurerm_resource_group.desktops_ka.location
  resource_group_name          = azurerm_resource_group.desktops_ka.name
  type                         = "Desktop"
  host_pool_id                 = azurerm_virtual_desktop_host_pool.dt_demo[0].id
  default_desktop_display_name = "Ascode Virtual Desktop Demo"
  friendly_name                = "Demo Desktops"
}

resource "azurerm_virtual_desktop_application_group" "remote_apps" {
  count               = local.pooled_count > 0 ? 1 : 0
  name                = "remote-apps"
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name
  type                = "RemoteApp"
  host_pool_id        = azurerm_virtual_desktop_host_pool.dt_pooled.0.id
  friendly_name       = "Applications"
}

resource "azurerm_virtual_desktop_workspace" "dt" {
  name                = "desktop-workspace"
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name
}

resource "azurerm_virtual_desktop_workspace_application_group_association" "workspace_dt_pooled" {
  count                = local.pooled_count > 0 ? 1 : 0
  workspace_id         = azurerm_virtual_desktop_workspace.dt.id
  application_group_id = azurerm_virtual_desktop_application_group.desktop_pooled.0.id
}

resource "azurerm_virtual_desktop_workspace_application_group_association" "workspace_dt_personal" {
  count                = local.personal_count > 0 ? 1 : 0
  workspace_id         = azurerm_virtual_desktop_workspace.dt.id
  application_group_id = azurerm_virtual_desktop_application_group.desktop_personal[0].id
}

resource "azurerm_virtual_desktop_workspace_application_group_association" "workspace_dt_demo" {
  count                = local.demo_count > 0 ? 1 : 0
  workspace_id         = azurerm_virtual_desktop_workspace.dt.id
  application_group_id = azurerm_virtual_desktop_application_group.desktop_demo.0.id
}

resource "azurerm_virtual_desktop_workspace_application_group_association" "workspace_remote_apps" {
  count                = local.pooled_count > 0 ? 1 : 0
  workspace_id         = azurerm_virtual_desktop_workspace.dt.id
  application_group_id = azurerm_virtual_desktop_application_group.remote_apps.0.id
}

resource "azurerm_role_assignment" "pooled_desktop_application_group_user" {
  for_each             = local.pooled_count > 0 ? local.pooled_desktop_users : {}
  scope                = azurerm_virtual_desktop_application_group.desktop_pooled.0.id
  role_definition_name = "Desktop Virtualization User"
  principal_id         = each.value
}

resource "azurerm_role_assignment" "demo_desktop_application_group_user" {
  for_each             = local.demo_count > 0 ? local.demo_desktop_users : {}
  scope                = azurerm_virtual_desktop_application_group.desktop_demo.0.id
  role_definition_name = "Desktop Virtualization User"
  principal_id         = each.value
}

resource "azurerm_role_assignment" "pooled_desktop_vm_admin" {
  for_each             = local.pooled_count > 0 ? local.pooled_desktop_admins : {}
  scope                = azurerm_windows_virtual_machine.w10_vm.0.id
  role_definition_name = "Virtual Machine Administrator Login"
  principal_id         = each.value
}

resource "azurerm_role_assignment" "demo_desktop_vm_admin" {
  for_each             = local.demo_count > 0 ? local.demo_desktop_admins : {}
  scope                = azurerm_windows_virtual_machine.w10_demo_vm.0.id
  role_definition_name = "Virtual Machine Administrator Login"
  principal_id         = each.value
}

resource "azurerm_role_assignment" "pooled_desktop_vm_user" {
  for_each             = local.pooled_count > 0 ? local.pooled_desktop_users : {}
  scope                = azurerm_windows_virtual_machine.w10_vm.0.id
  role_definition_name = "Virtual Machine User Login"
  principal_id         = each.value
}
resource "azurerm_role_assignment" "demo_desktop_vm_user" {
  for_each             = local.demo_count > 0 ? local.demo_desktop_users : {}
  scope                = azurerm_windows_virtual_machine.w10_demo_vm.0.id
  role_definition_name = "Virtual Machine User Login"
  principal_id         = each.value
}

resource "azurerm_role_assignment" "vendit_demo_desktop_vm_user" {
  for_each             = local.vendit_demo_count > 0 ? local.vendit_demo_desktop_users : {}
  scope                = azurerm_windows_virtual_machine.vendit_demo_vm.0.id
  role_definition_name = "Virtual Machine User Login"
  principal_id         = each.value
}
