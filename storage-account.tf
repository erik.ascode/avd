locals {
  storage_account_name              = "${var.customer_tla}${var.environment}dtkasa"
  wvd_profiles_storage_account_name = "${var.customer_tla}${var.environment}fsx"
  whitelist_ips = {
  }
}

resource "azurerm_storage_account" "desktops_ka" {
  name                      = local.storage_account_name
  resource_group_name       = azurerm_resource_group.desktops_ka.name
  location                  = var.az_region
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
  allow_blob_public_access  = true
  min_tls_version           = "TLS1_2"

  network_rules {
    default_action             = "Deny"
    ip_rules                   = [for k, v in local.whitelist_ips : (v)]
    virtual_network_subnet_ids = [var.subnet_ids]
  }

  identity {
    type = "SystemAssigned"
  }

  tags = local.tags
}

resource "azurerm_storage_container" "scripts" {
  name                  = "scripts"
  storage_account_name  = azurerm_storage_account.desktops_ka.name
  container_access_type = "private"
}

resource "azurerm_storage_container" "install" {
  name                  = "install"
  storage_account_name  = azurerm_storage_account.desktops_ka.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "windows10_config_pooled" {
  depends_on             = [azurerm_virtual_desktop_host_pool.dt_pooled]
  count                  = local.pooled_count > 0 ? 1 : 0
  name                   = "windows10ConfigPooled.ps1"
  storage_account_name   = azurerm_storage_account.desktops_ka.name
  storage_container_name = azurerm_storage_container.scripts.name
  type                   = "Block"
  source_content         = templatefile("${path.cwd}/templates/windows10Config.ps1", { tenant_id = local.tenant_id, pooled_registration_token = azurerm_virtual_desktop_host_pool.dt_pooled.0.registration_info[0].token })
}

resource "azurerm_storage_blob" "windows10_config_personal" {
  depends_on             = [azurerm_virtual_desktop_host_pool.dt_personal]
  count                  = local.personal_count > 0 ? 1 : 0
  name                   = "windows10ConfigDemo.ps1"
  storage_account_name   = azurerm_storage_account.desktops_ka.name
  storage_container_name = azurerm_storage_container.scripts.name
  type                   = "Block"
  source_content         = templatefile("${path.cwd}/templates/windows10ConfigPersonal.ps1", { tenant_id = local.tenant_id, personal_registration_token = azurerm_virtual_desktop_host_pool.dt_personal[0].registration_info[0].token })
}

resource "azurerm_storage_blob" "windows10_config_demo" {
  depends_on             = [azurerm_virtual_desktop_host_pool.dt_demo]
  count                  = local.demo_count > 0 ? 1 : 0
  name                   = "windows10ConfigDemo.ps1"
  storage_account_name   = azurerm_storage_account.desktops_ka.name
  storage_container_name = azurerm_storage_container.scripts.name
  type                   = "Block"
  source_content         = templatefile("${path.cwd}/templates/windows10ConfigDemo.ps1", { tenant_id = local.tenant_id, demo_registration_token = azurerm_virtual_desktop_host_pool.dt_demo[0].registration_info[0].token })
}
