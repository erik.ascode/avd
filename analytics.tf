locals {
  subscription_diagnostics = [
    {
      "category" = "Administrative"
      "enabled"  = "true"
    },
    {
      "category" = "Alert"
      "enabled"  = "true"
    },
    {
      "category" = "Autoscale"
      "enabled"  = "true"
    },
    {
      "category" = "Policy"
      "enabled"  = "true"
    },
    {
      "category" = "Recommendation"
      "enabled"  = "true"
    },
    {
      "category" = "ResourceHealth"
      "enabled"  = "true"
    },
    {
      "category" = "Security"
      "enabled"  = "true"
    },
    {
      "category" = "ServiceHealth"
      "enabled"  = "true"
    }
  ]
  pooled_desktops_diagnostics = [
    {
      "category" = "Checkpoint"
      "enabled"  = "true"
    },
    {
      "category" = "Error"
      "enabled"  = "true"
    },
    {
      "category" = "Management"
      "enabled"  = "true"
    },
    {
      "category" = "Connection"
      "enabled"  = "true"
    },
    {
      "category" = "HostRegistration"
      "enabled"  = "true"
    },
    {
      "category" = "AgentHealthStatus"
      "enabled"  = "true"
    }
  ]
  personal_desktops_diagnostics = [
    {
      "category" = "Checkpoint"
      "enabled"  = "true"
    },
    {
      "category" = "Error"
      "enabled"  = "true"
    },
    {
      "category" = "Management"
      "enabled"  = "true"
    },
    {
      "category" = "Connection"
      "enabled"  = "true"
    },
    {
      "category" = "HostRegistration"
      "enabled"  = "true"
    },
    {
      "category" = "AgentHealthStatus"
      "enabled"  = "true"
    }
  ]
  pooled_application_groups_diagnostics = [
    {
      "category" = "Checkpoint"
      "enabled"  = "true"
    },
    {
      "category" = "Error"
      "enabled"  = "true"
    },
    {
      "category" = "Management"
      "enabled"  = "true"
    }
  ]
  personal_application_groups_diagnostics = [
    {
      "category" = "Checkpoint"
      "enabled"  = "true"
    },
    {
      "category" = "Error"
      "enabled"  = "true"
    },
    {
      "category" = "Management"
      "enabled"  = "true"
    }
  ]
  virtual_desktop_workspace_diagnostics = [
    {
      "category" = "Checkpoint"
      "enabled"  = "true"
    },
    {
      "category" = "Error"
      "enabled"  = "true"
    },
    {
      "category" = "Management"
      "enabled"  = "true"
    },
    {
      "category" = "Feed"
      "enabled"  = "true"
    }
  ]
}

resource "azurerm_resource_group" "ka_analytics" {
  name     = "ka-analytics-rg-${var.environment}"
  location = var.az_region
}

resource "azurerm_log_analytics_workspace" "ka_analytics" {
  name                = "ka-analytics-workspace-${var.environment}"
  location            = azurerm_resource_group.ka_analytics.location
  resource_group_name = azurerm_resource_group.ka_analytics.name
  sku                 = "PerGB2018"
  retention_in_days   = var.environment != "prd" ? 30 : 90
}

resource "azurerm_storage_account" "ka_analytics" {
  name                      = "${var.customer_tla}analyticssa${var.environment}ka"
  resource_group_name       = azurerm_resource_group.ka_analytics.name
  location                  = var.az_region
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
  min_tls_version           = "TLS1_2"

  #   network_rules {
  #     default_action             = "Deny"
  #     ip_rules                   = [for k, v in var.whitelist_ips : (v)]
  #     virtual_network_subnet_ids = [data.terraform_remote_state.gcs["net"].outputs.subnets_ka["private"].id, data.terraform_remote_state.gcs["net"].outputs.subnets_ka["public"].id, data.terraform_remote_state.gcs["net"].outputs.subnets_ka["database"].id]
  #   }

  identity {
    type = "SystemAssigned"
  }

  tags = local.tags
}

resource "azurerm_monitor_diagnostic_setting" "subscription" {
  name                       = "subscription-diag-${data.azurerm_subscription.current.subscription_id}"
  target_resource_id         = data.azurerm_subscription.current.id
  storage_account_id         = azurerm_storage_account.ka_analytics.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.ka_analytics.id

  dynamic "log" {
    for_each = local.subscription_diagnostics
    content {
      category = log.value.category
      enabled  = log.value.enabled
    }
  }
}

resource "azurerm_monitor_diagnostic_setting" "ka_keyvault" {
  name                       = "ka-keyvault-diag-${var.environment}"
  target_resource_id         = azurerm_key_vault.desktops_ka.id
  storage_account_id         = azurerm_storage_account.ka_analytics.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.ka_analytics.id

  log {
    category = "AuditEvent"
    enabled  = true

    retention_policy {
      enabled = true
      days    = var.environment != "prd" ? 30 : 3650
    }
  }
  log {
    category = "AzurePolicyEvaluationDetails"
    enabled  = false
    retention_policy {
      enabled = false
      days    = 0
    }
  }

  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = true
      days    = var.environment != "prd" ? 30 : 365
    }
  }
}

resource "azurerm_monitor_diagnostic_setting" "ka_pooled_desktops" {
  count                      = local.pooled_count > 0 ? 1 : 0
  name                       = "pooled-desktops-diag-${var.environment}"
  target_resource_id         = azurerm_virtual_desktop_host_pool.dt_pooled.0.id
  storage_account_id         = azurerm_storage_account.ka_analytics.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.ka_analytics.id

  dynamic "log" {
    for_each = local.pooled_desktops_diagnostics
    content {
      category = log.value.category
      enabled  = log.value.enabled

      retention_policy {
        enabled = true
        days    = var.environment != "prd" ? 30 : 3650
      }
    }
  }
}

resource "azurerm_monitor_diagnostic_setting" "ka_personal_desktops" {
  count                      = local.personal_count > 0 ? 1 : 0
  name                       = "personal-desktops-diag-${var.environment}"
  target_resource_id         = azurerm_virtual_desktop_host_pool.dt_personal[0].id
  storage_account_id         = azurerm_storage_account.ka_analytics.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.ka_analytics.id
  #   log_analytics_destination_type = "Dedicated"

  dynamic "log" {
    for_each = local.personal_desktops_diagnostics
    content {
      category = log.value.category
      enabled  = log.value.enabled

      retention_policy {
        enabled = true
        days    = var.environment != "prd" ? 30 : 3650
      }
    }
  }
}

resource "azurerm_monitor_diagnostic_setting" "ka_pooled_application_groups" {
  count                      = local.pooled_count > 0 ? 1 : 0
  name                       = "pooled-application-groups-diag-${var.environment}"
  target_resource_id         = azurerm_virtual_desktop_application_group.desktop_pooled.0.id
  storage_account_id         = azurerm_storage_account.ka_analytics.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.ka_analytics.id

  dynamic "log" {
    for_each = local.pooled_application_groups_diagnostics
    content {
      category = log.value.category
      enabled  = log.value.enabled

      retention_policy {
        enabled = true
        days    = var.environment != "prd" ? 30 : 3650
      }
    }
  }
}

resource "azurerm_monitor_diagnostic_setting" "ka_personal_application_groups" {
  count                      = local.personal_count > 0 ? 1 : 0
  name                       = "personal-application-groups-diag-${var.environment}"
  target_resource_id         = azurerm_virtual_desktop_application_group.desktop_personal[0].id
  storage_account_id         = azurerm_storage_account.ka_analytics.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.ka_analytics.id

  dynamic "log" {
    for_each = local.personal_application_groups_diagnostics
    content {
      category = log.value.category
      enabled  = log.value.enabled

      retention_policy {
        enabled = true
        days    = var.environment != "prd" ? 30 : 3650
      }
    }
  }
}

resource "azurerm_monitor_diagnostic_setting" "ka_wvd_workspace" {
  name                       = "virtual-desktop-workspace-diag-${var.environment}"
  target_resource_id         = azurerm_virtual_desktop_workspace.dt.id
  storage_account_id         = azurerm_storage_account.ka_analytics.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.ka_analytics.id

  dynamic "log" {
    for_each = local.virtual_desktop_workspace_diagnostics
    content {
      category = log.value.category
      enabled  = log.value.enabled

      retention_policy {
        enabled = true
        days    = var.environment != "prd" ? 30 : 3650
      }
    }
  }
}
