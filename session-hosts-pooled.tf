# To get a list of Desktop VM Images, 'az vm image list -p "MicrosoftWindowsDesktop" --all -otable'
locals {
  w10_pooled = {
    name           = "wxpool"
    computer_name  = "wxpool"
    subnet_id      = var.subnet_id
    disk_size_gb   = 100
    instance_size  = "Standard_D8s_v4"
    admin_username = random_string.admin_user["w10_pooled"].result
    admin_password = random_password.admin_password["w10_pooled"].result
    source_image_reference = {
      publisher = "MicrosoftWindowsDesktop"
      offer     = "office-365"
      sku       = "21h1-evd-o365pp"
      version   = "latest"
    }
  }

  admin_users = {
    w10_pooled   = "w10pooledadmin"
    w10_demo     = "w10demoadmin"
    w10_personal = "w10personaladmin"
    vendit_demo  = "venditdemoadmin"
  }
  pooled_count = 1

}

##
## get a list of extensions: az vm extension image list
## 

# to be able to use the encryption at host feature for session hosts, the feature must be enabled.
# it requires the Azure CLI to be installed.
# make sure that the subscription you run this for is enabled
# you can check the subscriptions with 'az account list`
# the active subscription returns `"isDefault": true'
resource "null_resource" "encryption_at_host" {
  provisioner "local-exec" {
    command = "az feature register --namespace Microsoft.Compute --name EncryptionAtHost && az provider register -n Microsoft.Compute"
  }
}

resource "random_password" "admin_password" {
  for_each    = local.admin_users
  length      = 64
  min_special = 2
  min_upper   = 2
  min_numeric = 2
}

resource "random_string" "admin_user" {
  for_each = local.admin_users
  length   = 15
  special  = false
}

resource "azurerm_key_vault_secret" "desktops_ka" {
  for_each     = local.admin_users
  key_vault_id = azurerm_key_vault.desktops_ka.id

  name  = "${random_string.admin_user[each.key].result}-${each.value}"
  value = random_password.admin_password[each.key].result

  tags = local.tags
}

# Virtual Machines
# Pooled Session Host(s)
resource "azurerm_network_interface" "nic_w10" {
  count               = local.pooled_count
  name                = "${var.environment}-${local.w10_pooled.name}-${count.index + 1}-nic"
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = local.w10_pooled.subnet_id
    private_ip_address_allocation = "Dynamic"
  }

  tags = local.tags
}

resource "azurerm_windows_virtual_machine" "w10_vm" {
  count               = local.pooled_count
  depends_on          = [azurerm_key_vault_access_policy.disk_access, azurerm_disk_encryption_set.desktops_ka, null_resource.encryption_at_host]
  name                = "${var.environment}-${local.w10_pooled.name}-${count.index + 1}"
  location            = azurerm_resource_group.desktops_ka.location
  resource_group_name = azurerm_resource_group.desktops_ka.name

  size           = local.w10_pooled.instance_size
  admin_username = local.w10_pooled.admin_username
  admin_password = local.w10_pooled.admin_password

  network_interface_ids = [
    azurerm_network_interface.nic_w10[count.index].id,
  ]

  os_disk {
    name                   = "${var.environment}-${local.w10_pooled.name}-${count.index + 1}-OSDisk"
    caching                = "ReadWrite"
    storage_account_type   = "Premium_LRS"
    disk_encryption_set_id = azurerm_disk_encryption_set.desktops_ka.id
  }

  source_image_reference {
    publisher = local.w10_pooled.source_image_reference.publisher
    offer     = local.w10_pooled.source_image_reference.offer
    sku       = local.w10_pooled.source_image_reference.sku
    version   = local.w10_pooled.source_image_reference.version
  }

  boot_diagnostics {
    storage_account_uri = trimsuffix(tostring(azurerm_storage_account.desktops_ka.primary_blob_endpoint), "/${var.environment}-${local.w10_pooled.name}-${count.index + 1}")
  }

  identity {
    type = "SystemAssigned"
  }

  timezone = "W. Europe Standard Time"

  tags                       = local.tags
  encryption_at_host_enabled = true
}

resource "azurerm_monitor_diagnostic_setting" "w10_vm" {
  count                      = local.pooled_count
  name                       = "${azurerm_windows_virtual_machine.w10_vm[count.index].name}-diag"
  target_resource_id         = azurerm_windows_virtual_machine.w10_vm[count.index].id
  storage_account_id         = azurerm_storage_account.ka_analytics.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.ka_analytics.id
  #   log_analytics_destination_type = "Dedicated"
  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = true
      days    = var.environment != "prd" ? 30 : 365
    }
  }
}

resource "azurerm_virtual_machine_extension" "azure_ad_login" {
  count                = local.pooled_count
  name                 = "${local.w10_pooled.name}-${count.index + 1}-azure-ad-login"
  virtual_machine_id   = azurerm_windows_virtual_machine.w10_vm[count.index].id
  publisher            = "Microsoft.Azure.ActiveDirectory"
  type                 = "AADLoginForWindows"
  type_handler_version = "1.0"
  settings             = <<SETTINGS
  {
     "intune" : "true",
     "mdmId" : "0000000a-0000-0000-c000-000000000000"
  }
  SETTINGS
}

resource "azurerm_virtual_machine_extension" "w10_session_host_anti_malware" {
  depends_on = [azurerm_virtual_machine_extension.azure_ad_login]
  count      = local.pooled_count

  name                 = "${local.w10_pooled.name}-${count.index + 1}-anti-malware"
  virtual_machine_id   = azurerm_windows_virtual_machine.w10_vm[count.index].id
  publisher            = "Microsoft.Azure.Security"
  type                 = "IaaSAntimalware"
  type_handler_version = "1.3"

  settings = <<SETTINGS
      {
          "AntimalwareEnabled": "true",
          "Exclusions": {
            "Extensions": ".log;.ldf",
            "Paths": "%windir%\\SoftwareDistribution\\Datastore\\Edb*.jrs;%windir%\\SoftwareDistribution\\Datastore\\Edb.chk;%windir%\\SoftwareDistribution\\Datastore\\Tmp.edb;%windir%\\Security\\Database\\*.edb;%windir%\\Security\\Database\\*.sdb;%windir%\\Security\\Database\\*.log;%windir%\\Security\\Database\\*.chk;%windir%\\Security\\Database\\*.jrs;%windir%\\Security\\Database\\*.xml;%windir%\\Security\\Database\\*.csv;%windir%\\Security\\Database\\*.cmtx;%allusersprofile%\\NTUser.pol;%SystemRoot%\\System32\\GroupPolicy\\Machine\\Registry.pol;%SystemRoot%\\System32\\GroupPolicy\\Machine\\registry.tmp;%SystemRoot%\\System32\\GroupPolicy\\User\\registry.pol;%SystemRoot%\\System32\\GroupPolicy\\User\\registry.tmp;userprofile%\\NTUser.dat*",
            "Processes": "mssence.svc"
          },
          "RealtimeProtectionEnabled": "true",
          "ScheduledScanSettings": {
            "isEnabled": "true",
            "scanType": "Quick",
            "day": "7",
            "time": "120"
          }
      }
  SETTINGS
}

resource "azurerm_virtual_machine_extension" "windows10_config_pooled" {
  depends_on = [azurerm_virtual_machine_extension.azure_ad_login]
  count      = local.pooled_count

  name                 = "${local.w10_pooled.name}-${count.index + 1}-config"
  virtual_machine_id   = azurerm_windows_virtual_machine.w10_vm[count.index].id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.10"

  settings = <<SETTINGS
  { 
      "fileUris": [
          "${azurerm_storage_container.install.id}/language-packs/nl-nl.zip",
          "${azurerm_storage_blob.windows10_config_pooled[0].url}"
      ],
      "commandToExecute": "powershell.exe -ExecutionPolicy Unrestricted -file windows10ConfigPooled.ps1"
  }
  SETTINGS

  protected_settings = <<PROTECTEDSETTINGS
  {
      "storageAccountName": "${azurerm_storage_account.desktops_ka.name}",
      "storageAccountKey": "${azurerm_storage_account.desktops_ka.primary_access_key}"
  }
  PROTECTEDSETTINGS

  tags = local.tags
}
