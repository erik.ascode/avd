variable az_region {
  description = "Azure Region to deploy resource"
  type        = string
  default     = "westeurope"
}

variable gcp_region {
  description = "GCP Region to deploy resource"
  type        = string
  default     = "europe-west4"
}

variable "azure_subscription_ids" {
  type = map(string)
  default = {
    dev = "1234567"
  }
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "customer_tla" {
  type    = string
  default = "ascode"
}

variable "customer_domain" {
  type    = string
  default = "ascode.nl"
}

variable "gitlab_token" {
  type        = string
  description = "the Gitlab token to use for cloning repositories"
  default     = ""
}

variable "whitelist_ips" {
  type        = map(string)
  description = "a list of whitelisted IPs"
  default     = {}
}

variable admin_user {
    type = string
    description = "The user to use for adding the machine to the domain"
    default = ""
}

variable admin_password {
    type = string
    description = "The password to use for adding the machine to the domain"
    default = ""
}
