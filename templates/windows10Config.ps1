#Start transcript on VM being built, to ease troubleshooting
Start-Transcript "C:\Windows\Setup\Scripts\Windows10Config.txt"
$StopWatch = [system.diagnostics.stopwatch]::startNew()

# set the temporary path
$path = "C:\temp"

# check if the path exists, if not - create it
If (!(test-path $path)) {
    New-Item -ItemType Directory -Force -Path $path
}

# add Dutch language
$UserLanguageList = New-WinUserLanguageList -Language "en-US"
$UserLanguageList.Add("nl-NL")
Set-WinUserLanguageList -LanguageList $UserLanguageList -Force

# download and install prerequisites
# Virtual Desktop Agent
$url = "https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RWrmXv"
$output = "c:\temp\Microsoft.RDInfra.RDAgent.Installer-x64.msi"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Start-Process msiexec -ArgumentList "/i $output /qn /l*v c:\temp\vda_install.txt /norestart /passive REGISTRATIONTOKEN=${pooled_registration_token}" -wait

# Virtual Desktop Agent Bootloader
$url = "https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RWrxrH"
$output = "c:\temp\Microsoft.RDInfra.RDAgentBootLoader.Installer-x64.msi"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Start-Process msiexec -ArgumentList "/i $output /qn /l*v c:\temp\vda_boot_loader_install.txt" -wait

# Edge browser
# version 95.0.1020.44
# get .msi link on https://www.microsoft.com/en-us/edge/business/download
$url = "https://msedge.sf.dl.delivery.mp.microsoft.com/filestreamingservice/files/93862b4a-15ae-40a0-b501-f969633661e2/MicrosoftEdgeEnterpriseX64.msi"
$output = "c:\temp\MicrosoftEdgeEnterpriseX64.msi"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Start-Process msiexec -ArgumentList "/i $output /qn /l*v c:\temp\edge_install.txt" -wait

# Chocolatey (https://chocolatey.org/install)
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
Set-Variable "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

$Packages = 'notepadplusplus', 'brave'

ForEach ($PackageName in $Packages)
{
    choco install $PackageName -y
}

# make sure Office365 can be used in a shared computing environment
$SharedComputerLicensingKey = "HKLM:\Software\Microsoft\Office\ClickToRun\Configuration"
Set-ItemProperty -Path $SharedComputerLicensingKey -Name "SharedComputerLicensing" -Value 1 -Force

# Teams optimization
$TeamsWVDKey = "HKLM:\SOFTWARE\Microsoft\Teams"
Set-ItemProperty -Path $TeamsWVDKey -Name "IsWVDEnvironment" -Value 1 -Force

# Install the latest version of the Remote Desktop WebRTC Redirector Service
$url = "https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RWNg9F"
$output = "c:\temp\MsRdcWebRTCSvc_HostSetup_x64.msi"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Start-Process msiexec -ArgumentList "/i $output /qn /l*v c:\temp\webrtc_install.txt" -wait

# Install Teams
$url = "https://teams.microsoft.com/downloads/desktopurl?env=production&plat=windows&arch=x64&managedInstaller=true&download=true"
$output = "c:\temp\Teams_windows_x64.msi"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Start-Process msiexec -ArgumentList "/i $output /qn /l*v c:\temp\teams_install.txt ALLUSERS=1" -wait

# Install NuGet Package Provider
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force

# Install NL Language Pack
##Disable Language Pack Cleanup##
Disable-ScheduledTask -TaskPath "\Microsoft\Windows\AppxDeploymentClient\" -TaskName "Pre-staged app cleanup"

##Set Language Pack Content Stores##
$lp_install_files = "C:\Packages\Plugins\Microsoft.Compute.CustomScriptExtension\1.*\Downloads\0\language-packs\nl-nl.zip"
Expand-Archive "$lp_install_files" -DestinationPath $path -Force
Add-AppProvisionedPackage -Online -PackagePath $path\LanguageExperiencePack.nl-nl.Neutral.appx -LicensePath $path\License.xml
Add-WindowsPackage -Online -PackagePath $path\Microsoft-Windows-Client-Language-Pack_x64_nl-nl.cab
$LanguageList = Get-WinUserLanguageList
$LanguageList.Add("nl-nl")
Set-WinUserLanguageList $LanguageList -force

# Silently move Windows known folders to OneDrive
$OneDriveKey = "HKLM:\SOFTWARE\Policies\Microsoft\OneDrive"
Set-ItemProperty -Path $OneDriveKey -Name "KFMSilentOptIn" -Value "${tenant_id}" -Force

# Silently sign in users to the OneDrive sync app with their Windows credentials
Set-ItemProperty -Path $OneDriveKey -Name "SilentAccountConfig" -Value 1 -PropertyType DWORD -Force

# Prevent users from changing the location of their OneDrive folder
$OneDriveKey2 = "HKLM:\SOFTWARE\Policies\Microsoft\OneDrive\DisableCustomRoot"
Set-ItemProperty -Path $OneDriveKey2 -Name "${tenant_id}" -Value 1 -PropertyType DWORD -Force

# Prevent users from syncing personal OneDrive accounts
Set-ItemProperty -Path $OneDriveKey -Name "DisablePersonalSync" -Value 1 -PropertyType DWORD -Force

# Require users to confirm large delete operations
Set-ItemProperty -Path $OneDriveKey -Name "ForcedLocalMassDeleteDetection" -Value 1 -PropertyType DWORD -Force

# Prompt users when they delete multiple OneDrive files on their local computer (200 is default)
Set-ItemProperty -Path $OneDriveKey -Name "LocalMassDeleteFileDeleteThreshold" -Value 20 -Force

# Allow syncing OneDrive accounts for only specific organizations
Set-ItemProperty -Path $OneDriveKey -Name "AllowTenantList" -Value "${tenant_id}" -Force

# Clean up
Remove-Item $path\*.* -Recurse

$StopWatch.Stop()
$StopWatch.Elapsed.TotalMinutes
#Stop Transcript
Stop-Transcript
restart-computer -Confirm $false
